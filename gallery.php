<?php include('header.php')?>
<!--Pagetop Section Start-->
<section class="pagetop clearfix">
  <div class="container">
    <div class="pageTitle">
      <h3>Gallery</h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Gallery</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container clearfix">
    
    <div class="masonry-grid row">
								
								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/tibet-vista-day-tour.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/tibet-vista-day-tour.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/potala-palace.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/potala-palace.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/people_of_tibet.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/people_of_tibet.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/Laddakh-Buddhist-Festival-photo-credit-Ashwin-Bhardwaj-3.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/Laddakh-Buddhist-Festival-photo-credit-Ashwin-Bhardwaj-3.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/Gilt_roof_of_the_Jokhang.JPG" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/Gilt_roof_of_the_Jokhang.JPG" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->
                                <!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/2009111015118760.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/2009111015118760.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/Leh-ladakh-viajes-con-olaviajes.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/Leh-ladakh-viajes-con-olaviajes.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
					
								<!-- masonry grid item end -->
								
							

							</div>
  </div>
</section>
<!--Hero Section End--> 



<?php include('footer.php')?>