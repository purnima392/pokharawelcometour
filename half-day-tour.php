<?php include('header.php')?>
<!--Pagetop Section Start-->
<section class="pagetop clearfix">
  <div class="container">
    <div class="pageTitle">
      <h3>Half Day Tour</h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Half Day Tour</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container">
    <div class="roomDetail">
    <div class="row">
    	<div class="col-lg-6">
        	<img src="img/S_117938.jpg" alt="">
            
        </div>
        <div class="col-lg-6">
        	<div class="detail_title"><h3>Half Day Tour</h3>
        <p class="lead">Our tours are not private. Usually we get 2 to 3 people in a group, such as couples, friends and family members. But we also accept other guests, who are interested in  joining with our group.</p>
        	<h5>Prices start at: <b>$4,060</b> for 35 nights</h5>
        </div>
        <p>Saturday is our weekend in Nepal, therefore our carpet workshop is close, but our carpet showroom will be open. On Sunday, our Tibetan doctor is not available, due to weekend holiday, therefore we can’t able to meet with our Tibetan doctor.</p>
        <p>Our tour includes lots of information about Tibet, Buddhist culture and the living conditions of Tibetan people in Nepal. Also how well Tibetans have preserved their rich Buddhist cultural heritage, in Nepal, as refugees. Our guide will give informed explanation during the tour and will welcome questions from our guests as well.</p>
        <hr/>
        <h5>Program Includes:</h5>
        <ul class="list">
                <li>Discover the arts and crafts of the Tibetan Plateau – and learn how the Tibetan people introduced their unique skills in making Tibetan carpets in the settlements.</li>
                <li>Learn about the symbols and images that are so important in their daily life.</li>
                <li>Meet a Tibetan doctor and learn about Tibetan medicine and how it is used in treatment. There is also the opportunity to have first hand experience of a medical check up from a Tibetan doctor.</li>
              </ul>
        <div class="btn-wrapper"><a href="booking.php" class="btn btn-outline-warning">Book Now</a></div>
        </div>
    </div>
    	
    </div>
  </div>
</section>
<!--Hero Section End--> 

<!--Service Section Start--> 

<!--Room & Rates Start--> 

<!--Room & Rates End--> 
<!--Testimonial Start--> 

<!--Testimonial End-->

<?php include('footer.php')?>