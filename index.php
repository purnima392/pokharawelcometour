<?php include('header.php')?>
<!--Hero Section Start-->
<section class="heroSection">
  <div class="sliderBanner">
    <div class="flexslider">
      <ul class="slides">
        <li> <img src="img/people_of_tibet.jpg" alt="Slider">
          <div class="meta">
            <h1>A Brand New Hotel <span>Beyond Ordinary</span></h1>
            <h2>This is a perfect villa with spa center and hot tub for private, family and corporate rest in Le Marche region in Italy, with best nature views.</h2>
            <a href="about.php" class="btn btn-warning">Discover the full story</a> </div>
        </li>
        <li> <img src="img/Laddakh-Buddhist-Festival-photo-credit-Ashwin-Bhardwaj-3.jpg" alt="Slider">
          <div class="meta">
            <h1>A Brand New Hotel <span>Beyond Ordinary</span></h1>
            <h2>This is a perfect villa with spa center and hot tub for private, family and corporate rest in Le Marche region in Italy, with best nature views.</h2>
            <a href="about.php" class="btn btn-warning">Discover the full story</a> </div>
        </li>
      </ul>
    </div>
  </div>
  </div>
  </div>
</section>
<!--Hero Section End--> 

<!--About Section Start-->
<section class="aboutSection">
  <div class="container">
    <div class="row">
      <div class="col-lg-6"> <img src="img/Leh-ladakh-viajes-con-olaviajes.jpg" alt=""> </div>
      <div class="col-lg-6">
        <div class="introBlock">
          <div class="introContent">
            <h3>About Us</h3>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
            <a href="about.php" class="btn btn-outline-warning">Read More</a> </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--About Section End--> 
<!--Service Section Start-->
<section class="serviceSection graySection">
  <div class="container">
    <div class="title text-center">
      <h2>Cultural Tours</h2>
      <p> <span>Get a unique opportunity to spend time in local Tibetan communities and discover the cultural and spiritual life of Tibetans in Nepal today.</span></p>
    </div>
    <div class="row grid">
			<div class="col-lg-4">
				<figure class="effect-layla">
						<img src="img/tashi-palkhiel-monastry.jpg" alt="img06"/>
						<figcaption>
							<h2>Full Day Tour</h2>
							<p>When Layla appears, she brings an eternal summer along.</p>
							<a href="full-day-tour.php">View Detail</a>
						</figcaption>			
					</figure>
			</div>
			<div class="col-lg-4">
				<figure class="effect-layla">
						<img src="img/potala-palace.jpg" alt="img06"/>
						<figcaption>
							<h2>Half Day Tour</h2>
							<p>When Layla appears, she brings an eternal summer along.</p>
							<a href="half-day-tour.php">View Detail</a>
						</figcaption>			
					</figure>
			</div>
			<div class="col-lg-4">
				<figure class="effect-layla">
						<img src="img/Gilt_roof_of_the_Jokhang.JPG" alt="img06"/>
						<figcaption>
							<h2>Short Tours</h2>
							<p>When Layla appears, she brings an eternal summer along.</p>
							<a href="short-tour.php">View Detail</a>
						</figcaption>			
					</figure>
			</div>
	</div>
  </div>
</section>



<div class="top-footer">
  <div class="container custom-container clearfix">
    <h3 class="float-left">Follow us through social medai</h3>
    <ul class="float-right social_media">
      <li><a href="https://www.facebook.com/ranashotel"><i class="fa fa-facebook"></i><span><b>3,060</b>Followers</span></a></li>
      <li><a href="#"><i class="fa fa-twitter"></i><span><b>1,060</b>Followers</span></a></li>
      <li><a href="#"><i class="fa fa-tripadvisor"></i><span><b>2,060</b>Followers</span></a></li>
    </ul>
  </div>
</div>
<?php include('footer.php')?>