<?php include('header.php')?>
<!--Pagetop Section Start-->
<section class="pagetop clearfix">
  <div class="container">
    <div class="pageTitle">
      <h3>Paragliding</h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Paragliding</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container clearfix"> <div class="clearfix"><img src="img/Paragliding-in-Pokhara-750x390.jpg" alt="" class="img-align-right">
    <p>Paragliding in Pokhara can be understood as the best way to view the Annapurna Himalayan range from air and Pokhar city view with Lake fewa. Encouragingly, Pokhara has been nominated as the fifth Best location to Para glide in the world. Paragliding History is not very long in Nepal but now it is one of the “most do” game in Nepal. Paragliding can be a perfect alternative Adventure sport in Nepal. </p><br/>
	
	<b>There are few different courses to explore Paragliding</b><hr/>
	<ul class="list">
			<li><b>Tandem flight:</b> Tandem Flight  is for beginners to enjoy for half an hour.</li>
			<li><b>Cross country Flight: </b>Cross Country Flight is for those who have already experience and want to do more in the sky.</li>
			<li><b>Para Hawking:</b>Para Hawking is a para sailing with trained bird in the air.</li>
	</ul>
	</div>
   
  </div>
</section>
<!--Hero Section End--> 

<!--Service Section Start--> 

<!--Room & Rates Start--> 

<!--Room & Rates End--> 
<!--Testimonial Start--> 

<!--Testimonial End-->

<?php include('footer.php')?>