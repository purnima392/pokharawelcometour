<?php include('header.php')?>
<!--Pagetop Section Start-->
<section class="pagetop clearfix">
  <div class="container">
    <div class="pageTitle">
      <h3>Bungee Jump</h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Bungee Jump</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container clearfix"> <div class="clearfix"><img src="img/bungy-jump.jpg" alt="" class="img-align-right">
    <p>Bungy (Bungee) Jumping In Hemja, Pokhara, Nepal

Bungee jumping is one of the famous adventurous things to do while you are in Nepal. Previously there was only one place in Bhotekoshi,(brought to us by The Last Resort) Nepal to experience bungee jumping in Nepal but now you can also go to Pokhara for Bungee jumping. Pokhara is a most favorable place for internal and external tourist to visit. While you are in Pokhara don’t forget to gain an experience of Bungee jumping.</p>
	</div>
   
  </div>
</section>
<!--Hero Section End--> 

<!--Service Section Start--> 

<!--Room & Rates Start--> 

<!--Room & Rates End--> 
<!--Testimonial Start--> 

<!--Testimonial End-->

<?php include('footer.php')?>