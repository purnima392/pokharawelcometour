<?php include('header.php')?>
<!--Pagetop Section Start-->
<section class="pagetop clearfix">
  <div class="container">
    <div class="pageTitle">
      <h3>Sightseeing in Pokhara</h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Sightseeing in Pokhara</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container clearfix"> <div class="clearfix"><img src="img/pokhara-sightseeing-tours.jpg" alt="" class="img-align-right">
  <h5><b>The most popular sightseeing places in Pokhara Nepal.</b></h5><br/>
  <b>Phewa Lake</b>
    <p>Pokhara is one of the Nepal's most beautiful spots in Nepal. Phewa Tal is surrounded by a combination of monkey-filled forests and the high white peaks. The reflections in the mirror-like water in the early mornings are something you must see at least once before you die. Hire a boat and row yourself across the lake (or hire a local to do it for you - there are some fine times to be had on this lake, even in the middle of the night!)</p><br/>
	<b>Begnas Lake</b>
    <p>Begnan Lake is Located on Lekhnath. This is another small town east from Pokhara, Begnas Lake is out of town and away from the hustle, Begnas Tal is quiet, clean and peaceful. There are a few basic hotels to stay in and the odd refreshment shop. The walk along the road leading to Begnas Tal is fascinating, the seasonal rhythms of daily life in the country make great images. From May 2016 Bus service has been started between Fewa Lake and Begnas Lake. Every 30 minutes bus leaves from both Lake station. From Begnas, Bus leaves from Begnas Bus Park and from Fewa Lake, Bus leaves from Barahi Temple. It only cost Rs 50 per person.</p><br/>
	<b>Rupa Lake</b>
    <p>One of the more remote lakes in Pokhara with limited accessibility and hence more serene and unspoilt compared to the other lakes. A must visit for any nature lover</p>
	
	</div>
   
  </div>
</section>
<!--Hero Section End--> 

<!--Service Section Start--> 

<!--Room & Rates Start--> 

<!--Room & Rates End--> 
<!--Testimonial Start--> 

<!--Testimonial End-->

<?php include('footer.php')?>