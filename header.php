<!doctype html>
<html lang="en">
<head>
<title>Pokhara Welcome Tours Pvt.Ltd.</title>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!--Fonts-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Atomic+Age" rel="stylesheet">
<!--Icon Fonts-->
<link rel="stylesheet" type="text/css" href="css/flaticon.css">
<link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="css/animations.css" rel="stylesheet">
<link rel="stylesheet" href="css/flexslider.css">
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css"/>
<link rel="stylesheet" href="css/jquery-ui.css" type="text/css"/>
<link rel="stylesheet" href="css/daterangepicker.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">
</head>
<body>
	<!-- scrollToTop --> 
<!-- ================ -->
<div class="scrollToTop"><i class="fa fa-angle-up"></i></div>
<!--Header Start-->
<header id="header"> 
  <div class="container"><nav id="NavMenu" class="navbar navbar-expand-lg navbar-light sps sps--abv"> <a class="navbar-brand" href="index.php">Pokhara Welcome <span>Tours Pvt.Ltd.</span></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto float-right">
       
        <li class="nav-item active"> <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a> </li>
        <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About Us</a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown"> <a class="dropdown-item" href="guide-profile.php">Guide Profile</a><a class="dropdown-item" href="tibetaninnepal.php">Tibetan in Nepal</a> </div>
        </li>
      
        <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Cultural Tours</a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown"> <a class="dropdown-item" href="full-day-tour.php">Full Day Tour</a><a class="dropdown-item" href="half-day-tour.php">Half Day Tours</a> <a class="dropdown-item" href="short-tour.php">Short Tours</a> </div>
        </li>
        
        <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Activities</a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown"> <a class="dropdown-item" href="paragliding.php">Paragliding</a><a class="dropdown-item" href="bungee.php">Bungee Jump</a> <a class="dropdown-item" href="ultralight.php">Ultra Light</a><a class="dropdown-item" href="sightseeing.php">Sightseeing</a> </div>
        </li>
        <li class="nav-item"> <a class="nav-link" href="gallery.php">Gallery</a> </li>
        
        <li class="nav-item"> <a class="nav-link" href="contact.php">Contact Us</a> </li>
      </ul>
    </div>
  </nav></div>
</header>

<!--Header End--> 