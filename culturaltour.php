<?php include('header.php')?>
<!--Pagetop Section Start-->
<section class="pagetop clearfix">
  <div class="container">
    <div class="pageTitle">
      <h3>Cultural Tours</h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Cultural Tours</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container">
  	<div class="row">
    	<div class="col-lg-4">
        	<div class="roomList">
            	<div class="roomImg"><a href="full-day-tour.php"><img src="img/people_of_tibet.jpg" alt=""></a></div>
                <div class="roomInfo">
					<div class="roomTitle clearfix">
                	<h3><a href="full-day-tour.php">Full Day Tour</a></h3>
					
					</div>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                    <a href="full-day-tour.php" class="btn btn-outline-warning">View Detail</a>
                </div>
            </div>
        </div>
		<div class="col-lg-4">
        	<div class="roomList">
            	<div class="roomImg"><a href="half-day-tour.php"><img src="img/people_of_tibet.jpg" alt=""></a></div>
                <div class="roomInfo">
					<div class="roomTitle clearfix">
                	<h3><a href="half-day-tour.php">Half Day Tour</a></h3>
					
					</div>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                    <a href="half-day-tour.php" class="btn btn-outline-warning">View Detail</a>
                </div>
            </div>
        </div>
		<div class="col-lg-4">
        	<div class="roomList">
            	<div class="roomImg"><a href="short-tour.php"><img src="img/people_of_tibet.jpg" alt=""></a></div>
                <div class="roomInfo">
					<div class="roomTitle clearfix">
                	<h3><a href="short-tour.php">Short Tour</a></h3>
					
					</div>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                    <a href="short-tour.php" class="btn btn-outline-warning">View Detail</a>
                </div>
            </div>
        </div>
		
		
		
    </div>
  </div>
</section>
<!--Hero Section End--> 


<?php include('footer.php')?>