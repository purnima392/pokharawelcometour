<?php include('header.php')?>
<!--Pagetop Section Start-->
<section class="pagetop clearfix">
  <div class="container">
    <div class="pageTitle">
      <h3>Ultra Light</h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Ultra Light</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container clearfix"> <div class="clearfix"><img src="img/1.jpg" alt="" class="img-align-right">
    <p>Pokhara is known as “the jewel in the Himalaya”, a place of remarkable natural beauty. Situated at an altitude of 827m above sea level and 200km west of Kathmandu (Capital of Nepal) valley, the city is known as a Centre of Adventure. The serenity of lakes and the magnificence of the Himalayas rising behind them create an ambience of peace and magic. So, today the city has not only become the starting point for the most popular trekking and rafting destinations but also a place to relax and enjoy nature in all it’s beauty.</p>
	
	
	</div>
   
  </div>
</section>
<!--Hero Section End--> 

<!--Service Section Start--> 

<!--Room & Rates Start--> 

<!--Room & Rates End--> 
<!--Testimonial Start--> 

<!--Testimonial End-->

<?php include('footer.php')?>